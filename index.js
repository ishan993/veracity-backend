import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import supervisorController from './src/controllers/supervisorController';
import recipientController from './src/controllers/recipientController';
import messageController from './src/controllers/messageController';

const app = express();
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());


app.get('/hello', (req, res) => {
  console.log('Hello World!');
  res.json({ message: 'Hello World!' });
});

supervisorController(app);
recipientController(app);
messageController(app);

app.listen(process.env.PORT || 8080);
