import bodyParser from 'body-parser';
import { getRecipients } from '../services/recipientService';

const recipientController = (app) => {
  app.get('/recipients', (req, res) => {
    getRecipients().then((response) => {
      res.statusCode = 200;
      res.json({ result: response });
    }).catch((error) => {
      res.statusCode = error.code;
      res.json({ message: error.message });
    });
  });
};

export default recipientController;
