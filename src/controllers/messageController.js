import bodyParser from 'body-parser';
import { getMessages, sendMessage } from '../services/messageService';

const messageController = (app) => {
  app.get('/messages', (req, res) => {
    console.log('hitting message controller!');
    getMessages().then((response) => {
      console.log('GET_MESSAGE_CONTROLLER_SUCCESS: ' + response);
      res.json({ result: response });
    }).catch((error) => {
      console.log('GET_MESSAGE_CONTROLLER_ERROR: ' + error);
      res.json({ message: error });
    });
  });

  app.post('/messages', (req, res) => {
    console.log('I got these params' + JSON.stringify(req.body));
    const { supervisorId, recipientId, message } = req.body;
    
    if (supervisorId === undefined || supervisorId === '' || recipientId === '' ||
         recipientId === undefined || message === '' || message === undefined) {
      res.statusCode = 400;
      res.json({
        message: 'Invalid message params!',
      });
      return;
    }

    sendMessage({ supervisorId, recipientId, message }).then((response) => {
      console.log('SEND_MESSAGE_CONTROLLER_SUCCESS: ' + JSON.stringify(response));
      const messageObj = {};
      res.json({ result: response });
    }).catch((error) => {
      console.log('SEND_MESSAGE_CONTROLLER_ERROR: ' + JSON.stringify(error));
      res.json({ message: error });
    });
  });
};
export default messageController;
