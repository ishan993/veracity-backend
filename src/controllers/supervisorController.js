import bodyParser from 'body-parser';
import { getSupervisors } from '../services/supervisorService';

const supervisorController = (app) => {
  app.get('/supervisors', (req, res) => {
    getSupervisors().then((response) => {
      console.log('SUPERVISOR_CONTROLLER_SUCCESS');
      res.json({ result: response });
    }).catch((error) => {
      res.statusCode = error.code;
      res.json({ message: error.message });
    });
  });
};

export default supervisorController;
