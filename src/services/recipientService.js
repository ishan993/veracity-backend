import axios from 'axios';
import { serverConfig } from '../config';

export const getRecipients = () => {
  const { ROOT_URL, API_KEY } = serverConfig;
  const URL = ROOT_URL + '/' + API_KEY + '/list-recipients';
  const recipientReq = axios.get(URL);
  return recipientReq.then((response) => {
    console.log('GET_RECIPIENTS_SUCCESS');
    return Promise.resolve(response.data);
  }).catch((error) => {
    console.log('GET_RECIPIENTS_ERROR' + error.response.data.message);
    return Promise.reject(error.response.data);
  });
};

