import axios from 'axios';
import { serverConfig } from '../config';

export const getSupervisors = () => {
  const { ROOT_URL, API_KEY } = serverConfig;
  const URL = ROOT_URL + '/' + API_KEY + '/list-supervisors';
  const supervisorReq = axios.get(URL);
  return supervisorReq.then((response) => {
    return Promise.resolve(response.data);
  }).catch((error) => {
    console.log('GET_SUPERVISOR_ERROR' + error.response.data.message);
    return Promise.reject(error.response.data);
  });
};

