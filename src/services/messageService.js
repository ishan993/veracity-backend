import axios from 'axios';
import { serverConfig } from '../config';

export const getMessages = () => {
  const { ROOT_URL, API_KEY } = serverConfig;
  const URL = ROOT_URL + '/' + API_KEY + '/list-messages';
  const messageReq = axios.get(URL);
  return messageReq.then((response) => {
    console.log('GET_MESSAGES_SUCCESS');
    return Promise.resolve(response.data);
  }).catch((error) => {
    console.log('GET_MESSAGES_ERROR' + error.response.data.message);
    return Promise.reject(error.response.data);
  });
};

export const sendMessage = (messageObj) => {
  const { ROOT_URL, API_KEY } = serverConfig;
  const URL = ROOT_URL + '/' + API_KEY + '/send-message';

  const sendMessageReq = axios.post(URL, messageObj);
  return sendMessageReq.then((response) => {
    console.log('SEND_MESSAGE_SUCCESS' + JSON.stringify(response.data));
    return Promise.resolve(response.data);
  }).catch((error) => {
    console.log('SEND_MESSAGE_ERROR' + JSON.stringify(error.response.data));
    return Promise.reject('I got an error!');
  });
};

